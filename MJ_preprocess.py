###########
# imports #
###########
from pathlib import Path
import pickle
import os
from tqdm import tqdm
from joblib import Parallel, delayed
from collections import Iterable
import pandas as pd
import torchaudio
from itertools import compress
import numpy as np
import warnings
import glob
import datetime

from asr.CordioLexicon import Lexicon
from infrastructure.patientsInformation.CordioPatientClinicalInformation import CordioClinicalInformation
from infrastructure.CordioFile import CordioFile
from infrastructure.CordioExtractASRFeatures import readASRJsonPhoneme

class Preprocess:
    """
        ****************************************************************************
        Description:
         Preprocessing data to be used in Mockingjay peoject

        Input:
            db_path: path to databse (r'\\192.168.55.210\db', r'\\192.168.55.210\db\Acute', ...)
            patient_list: list of patients codes (["RAM-0051", "RAM-0057", "RAM-0079"])
            sentence_List: list of sentences
            phonemeId_list: list of phoneme asr ids or triphone strings
            lexiconPath: path to CordioLexiconVariations.csv (r"E:\Or_rnd\code\python\asr\asrserver\asr\CordioLexiconVariations.csv")
            workers: number of workers for parallel computing. use -1 for automatic maximum number of workers

        Usage example:
            # db_path = r'\\192.168.55.210\db\Acute'
              pl = ["RAM-0053", "RAM-0072", "RAM-0077",
                    "RAM-0051", "RAM-0057", "RAM-0079",
                    "RAM-0059", "RAM-0066", "RAM-0081"]
              pp = Preprocess(db_path=db_path, patient_list=pl)
              phn_tensorWavforms_list, ClinicalInfoPaths_df = pp.getPhnTensorWavformsList(phn_id="min", device='cuda',
                                                                                          downsample_rate=16000,
                                                                                          clinicallInfo='all',
                                                                                          disable_pbs=False)

         Cordio Medical - Confidential
         Version: 0.11    2021-01-17

         Revision History:
         |   Ver    | Author    | Date           | Change Description
         |----------|-----------|----------------|--------------------
         |   0.1    | Or        | 2021-01-07     | Initial
         |   0.11   | Or        | 2021-01-17     | added sentence filtering to getSentenceDownSampledTensorWavformList
         |   x.x    | xxxx      | xxxx-xx-xx     | x

        ****************************************************************************
        """

    def __init__(self, db_path:str=r'\\192.168.55.210\db', patient_list:list=None, sentence_List:list=None, phonemeId_list:list=None,
                 lexiconPath=r"E:\Or_rnd\code\python\asr\asrserver\asr\CordioLexiconVariations.csv", workers=1):

        self.db_path = Path(db_path)
        self.patient_list = patient_list
        self.phonemeId_list = phonemeId_list
        self.__lexicon = Lexicon(LexiconCSV=Path(lexiconPath))
        self.__phonemeStr_list = None
        self.sentence_list = sentence_List
        self.workers = workers

        self.__wavPaths = None
        self.__ClinicalInfoPaths_df = None

    ###############
    # properties: #
    ###############
    @property
    def phonemeStr_list(self):
        if self.__phonemeStr_list is None:
            if self.phonemeId_list is not None:
                self.__phonemeStr_list = [self.__lexicon.get_triphone_id(phonemeId) for phonemeId in self.phonemeId_list]
        return self.__phonemeStr_list

    @property
    def wavPaths(self):
        if self.__wavPaths is None:
            self.__wavPaths = self.__getWavFiles__()
        return self.__wavPaths

    @property
    def ClinicalInfoPaths_df(self):
        if self.__ClinicalInfoPaths_df is None:
            self.__ClinicalInfoPaths_df = self.__getDryWetData_df__()
        return self.__ClinicalInfoPaths_df

    ###########
    # Methods #
    ###########

    def __flatten_list__(self, lis, stopType="str"):
        def flatten(lis, stopType="str"):
            """
            convert a nested list into a one dimensional list

            Works for any level of nesting:
            a = [1,[2,2,[2]],4]
            list(flatten(a))
            out:
            [1, 2, 2, 2, 4]
            """
            for item in lis:
                # if type(item).__name__ == stopType:
                #     print(stopType)
                # if isinstance(item, Iterable) and not isinstance(item, stopType):
                if isinstance(item, Iterable) and type(item).__name__ != stopType:
                    for x in flatten(item, stopType):
                        yield x
                else:
                    yield item

        return list(flatten(lis, stopType))

    def __getAllRelevantDbFiles__(self):
        # get all HMOs paths:
        allHMOs = [self.db_path / Path(directory) for directory in os.listdir(str(self.db_path)) if
                   os.path.isdir(str(self.db_path / directory)) and directory.isupper() and len(directory) == 3]
        # get all patients paths:
        allPatients = []
        for HMO, i in zip(allHMOs, tqdm(range(len(allHMOs)))):
            if self.patient_list is not None:
                allPatients.extend([HMO / Path(directory) for directory in os.listdir(str(HMO)) if
                                    os.path.isdir(str(Path(HMO) / Path(directory))) and directory in self.patient_list])
            else:
                allPatients.extend([HMO / Path(directory) for directory in os.listdir(str(HMO)) if
                                    os.path.isdir(str(Path(HMO) / Path(directory)))])

        # for acute data:
        if 'Acute' in str(self.db_path):
            allPatients = [currPath / "cut" for currPath in allPatients]

        # get all files paths:
        # allFiles = []
        # for patient, i in zip(allPatients, tqdm(range(len(allPatients)))):
        #     allFiles.extend([patient / Path(directory) for directory in os.listdir(str(patient)) if not os.path.isdir(str(Path(patient) / Path(directory)))])

        def Parllelfunc(patient):
            patientDir_list = os.listdir(str(patient))
            retList = [patient / Path(directory) for directory in patientDir_list if
                       not os.path.isdir(str(Path(patient) / Path(directory)))]
            return retList

        allFiles = Parallel(n_jobs=1)(
            delayed(Parllelfunc)(allPatients[i]) for i in tqdm(range(len(allPatients)), desc="get all files paths..."))

        return allFiles

    def __getFiles__(self):
        filesList = self.__getAllRelevantDbFiles__()
        filesList = self.__flatten_list__(filesList, stopType="str")
        return filesList

    def __getWavFiles__(self):
        allFiles = self.__getFiles__()
        wavFiles = [file for file in allFiles if ".wav" in str(file)]
        return wavFiles

    def __getPatientDryWetData_df__(self, patientWavFilePaths, patient, experiment, sentences: list=None, getDF=False):
        # get dry wet data for patient:

        # filter by sentences:
        if sentences is not None:
            patientNsentencesWavFilePaths = []
            for sentence in sentences:
                tmpList = [wp for wp in patientWavFilePaths if sentence in wp]
                patientNsentencesWavFilePaths.extend(tmpList)

        clinicalInfoObj = CordioClinicalInformation(patient, experiment)
        ci_num2str = {1: 'wet', 0.99: 'extremeWet', 0.5: 'unknown', 0: 'extremeDry', 0.01: 'dry'}
        df_dict_list = []
        for wav_path in patientWavFilePaths:
            f = CordioFile(wav_path)
            ci_num = clinicalInfoObj(f.recordingDatetime)
            # if ci_num not in list(ci_num2str.keys()):
            #     sum+=1
            # else:
            ci_str = ci_num2str[ci_num]
            df_dict_list.append({'patient': patient, 'file': wav_path, 'clinicalInfo': ci_str})
        if getDF:
            df = pd.DataFrame.from_dict(df_dict_list)
            df = df[df['clinicalInfo'] != 'unknown']
            return df
        else:
            return df_dict_list

    def __getDryWetData_df__(self):
        """
        # can be made to run in parallel per patient
        :return: df containing wav files and clinical info labeling
        """
        df_dict_list = []
        for patient in self.patient_list:
            patientWavFilePaths = [wp for wp in self.wavPaths if patient in str(wp)]
            experiment = 'acute' if 'Acute' in str(patientWavFilePaths[0]) else 'community'
            tmpDictList = self.__getPatientDryWetData_df__(patientWavFilePaths, patient, experiment,
                                                       sentences=self.sentence_list, getDF=False)

            df_dict_list.extend(tmpDictList)

        df = pd.DataFrame.from_dict(df_dict_list)
        df = df[df['clinicalInfo'] != 'unknown'].reset_index(drop=True) # include only wet or dry data
        return df

    def getMultiSentenceDownSampledTensorWavformList(self, device, downsample_rate, sentence_list=None, clinicallInfo: str= 'all', get_ClinicalInfoPaths_df: bool=False, disable_pbs: bool=True):

        # filter data by clinical information:
        if clinicallInfo == 'all': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df
        if clinicallInfo == 'extremeDry': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df[self.ClinicalInfoPaths_df['clinicalInfo']=='extremeDry'].reset_index(drop=True)
        if clinicallInfo == 'dry': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df[self.ClinicalInfoPaths_df['clinicalInfo']=='dry'].reset_index(drop=True)
        if clinicallInfo == 'unknown': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df[self.ClinicalInfoPaths_df['clinicalInfo']=='unknown'].reset_index(drop=True)
        if clinicallInfo == 'wet': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df[self.ClinicalInfoPaths_df['clinicalInfo']=='wet'].reset_index(drop=True)
        if clinicallInfo == 'extremeWet': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df[self.ClinicalInfoPaths_df['clinicalInfo']=='extremeWet'].reset_index(drop=True)

        # filter data by sentence:
        def sentenceValueManegment(sentence):
            if isinstance(sentence, str):
                if "S" not in sentence or len(sentence) != 5:
                    raise ValueError('given sentence is not integer and not in the right string format')
                sentenceFilter = sentence
            elif isinstance(sentence, int):
                sentenceFilter = self.__lexicon.get_sentence_str(sentence)
            else:
                raise ValueError('given sentence value is not integer and not string')
            return sentenceFilter
        if sentence_list is not None:
            sentencesFilters = [sentenceValueManegment(sentence) for sentence in sentence_list]
            filterMask = np.array([False]*len(curr_ClinicalInfoPaths_df))
            for sentencesFilter in sentencesFilters:
                tmp_filterMask = [sentencesFilter in str(curr_ClinicalInfoPath) for curr_ClinicalInfoPath in curr_ClinicalInfoPaths_df['file']]
                filterMask = filterMask | tmp_filterMask
            curr_ClinicalInfoPaths_df = curr_ClinicalInfoPaths_df[filterMask].reset_index(drop=True)

        # parallel:
        def tensorWavform_iteration(file):
            # file = curr_ClinicalInfoPaths_df['file'][i]
            waveform, sample_rate = torchaudio.load(file)
            downsample_resample = torchaudio.transforms.Resample(
                sample_rate, downsample_rate, resampling_method='sinc_interpolation')
            down_sampled_waveform = downsample_resample(waveform)
            return down_sampled_waveform[0].to(device)
        tensorWavformList = Parallel(n_jobs=self.workers)(
            delayed(tensorWavform_iteration)(curr_ClinicalInfoPaths_df['file'][i])
            for i in tqdm(range(len(curr_ClinicalInfoPaths_df)), desc='creating tensor waveform list', disable=disable_pbs))

        # add sentence id to df:
        sentence_id_list = []
        for filePath in curr_ClinicalInfoPaths_df["file"]:
            fileSentence = CordioFile(str(filePath)).sentence
            sentence_id_list.append(self.__lexicon.get_sentence_id(fileSentence))
        curr_ClinicalInfoPaths_df["sentenceId"] = sentence_id_list

        # return
        if get_ClinicalInfoPaths_df:
            return tensorWavformList, curr_ClinicalInfoPaths_df
        else:
            return tensorWavformList

    def getPhnDownSampledTensorWavformList(self, device, downsample_rate, phn_id, clinicallInfo: str= 'all', get_ClinicalInfoPaths_df: bool=False, disable_pbs: bool=True):
        if isinstance(phn_id, str):
            phn_id = self.__lexicon.get_triphone_id(phn_id)

        if clinicallInfo == 'all': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df
        if clinicallInfo == 'extremeDry': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df[self.ClinicalInfoPaths_df['clinicalInfo']=='extremeDry'].reset_index(drop=True)
        if clinicallInfo == 'dry': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df[self.ClinicalInfoPaths_df['clinicalInfo']=='dry'].reset_index(drop=True)
        if clinicallInfo == 'unknown': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df[self.ClinicalInfoPaths_df['clinicalInfo']=='unknown'].reset_index(drop=True)
        if clinicallInfo == 'wet': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df[self.ClinicalInfoPaths_df['clinicalInfo']=='wet'].reset_index(drop=True)
        if clinicallInfo == 'extremeWet': curr_ClinicalInfoPaths_df = self.ClinicalInfoPaths_df[self.ClinicalInfoPaths_df['clinicalInfo']=='extremeWet'].reset_index(drop=True)

        def cutPhnFromSentence(file, waveform, phn_id):
            if isinstance(phn_id, str):
                phn_id = self.lexicon.get_triphone_id(phn_id)
            # getting phoneme part of wav file:
            f = CordioFile(file)
            phonemeObjs, isValidPhn = readASRJsonPhoneme(f.asrJsonPath)
            if not isValidPhn:
                return False
            for p in phonemeObjs:
                if p.asr_expected_id == phn_id:
                    phnObj = p
                    break
            if 'phnObj' not in locals(): return False
            phn_Wavform = waveform[0, phnObj.start_sample: phnObj.end_sample]

            return phn_Wavform

        def tensorWavform_iteration(file, phn_id):
            if isinstance(phn_id, str):
                phn_id = self.lexicon.get_triphone_id(phn_id)
            # file = curr_ClinicalInfoPaths_df['file'][i]
            waveform, sample_rate = torchaudio.load(file)
            phn_Wavform = cutPhnFromSentence(file, waveform, phn_id) # filtering phoneme
            if isinstance(phn_Wavform, bool): return phn_Wavform
            downsample_resample = torchaudio.transforms.Resample(
                sample_rate, downsample_rate, resampling_method='sinc_interpolation')
            down_sampled_phn_Wavform = downsample_resample(phn_Wavform)
            return down_sampled_phn_Wavform.to(device)

        # tensorWavformList = Parallel(n_jobs=self.workers)(
        #     delayed(tensorWavform_iteration)(curr_ClinicalInfoPaths_df['file'][i], phn_id)
        #     for i in tqdm(range(len(curr_ClinicalInfoPaths_df)), desc='creating tensor waveform list'))

        tensorWavformList = []
        for i in tqdm(range(len(curr_ClinicalInfoPaths_df)), desc='creating tensor waveform list', disable=disable_pbs):
            tensorWavformList.extend([tensorWavform_iteration(curr_ClinicalInfoPaths_df['file'][i], phn_id)])
        # filter non-relevant waveforms:
        nonFalseIdx = [not isinstance(twf, bool) for twf in tensorWavformList]
        tensorWavformList = [tensorWavformList[i] for i in range(len(tensorWavformList)) if nonFalseIdx[i]]
        curr_ClinicalInfoPaths_df = curr_ClinicalInfoPaths_df[nonFalseIdx].reset_index(drop=True)
        # add phn id to df:
        curr_ClinicalInfoPaths_df["phonemeId"] = [phn_id] * len(curr_ClinicalInfoPaths_df)
        # return:
        if get_ClinicalInfoPaths_df:
            return tensorWavformList, curr_ClinicalInfoPaths_df
        else:
            return tensorWavformList

    def getMultiPhnDownSampledTensorWavformList(self, device, downsample_rate, phn_id_list, clinicallInfo: str = 'all',
                                           get_ClinicalInfoPaths_df: bool = False, disable_pbs: bool = True):
        # input management:
        for i in range(len(phn_id_list)):
            if isinstance(phn_id_list[i], str):
                phn_id_list[i] = self.__lexicon.get_triphone_id(phn_id_list[i])
        # getting data:
        allPhnForAllPatients_tensorWavformList = []
        tmp_allPhnForAllPatients_ClinicalInfoPaths_df = []
        pb = tqdm(range(len(phn_id_list)), desc='getting phoneme data', disable=disable_pbs)
        for i in pb:
            curr_tensorWavformList, curr_ClinicalInfoPaths_df_list = self.getPhnDownSampledTensorWavformList\
                (device, downsample_rate, phn_id=phn_id_list[i], clinicallInfo=clinicallInfo,
                 get_ClinicalInfoPaths_df=True, disable_pbs=True)
            allPhnForAllPatients_tensorWavformList.extend(curr_tensorWavformList)
            tmp_allPhnForAllPatients_ClinicalInfoPaths_df.append(curr_ClinicalInfoPaths_df_list)
        # reassemble df:
        allPhnForAllPatients_ClinicalInfoPaths_df = \
            pd.concat(tmp_allPhnForAllPatients_ClinicalInfoPaths_df).reset_index(drop=True)

        # return:
        if get_ClinicalInfoPaths_df:
            return allPhnForAllPatients_tensorWavformList, allPhnForAllPatients_ClinicalInfoPaths_df
        else:
            return allPhnForAllPatients_tensorWavformList

    def __getSinglePhnTensorWavformsList__(self, phn_id, tensorWavformList, ClinicalInfoPaths_df, clinicallInfo: str='all', disable_pbs: bool=True):

        if isinstance(phn_id, str):
            if len(phn_id) != 3 and len(phn_id) != 5:
                raise ValueError("phn_id is triphone and needs to be 3 letters or an asr_id ('min', /m-i+n/ or 100070207)")
            phn_id = self.__lexicon.get_triphone_id(phn_id)

        # add triphone id column
        ClinicalInfoPaths_df['phn_id'] = [phn_id] * len(ClinicalInfoPaths_df)
        phn_str = self.__lexicon.get_triphone_str(phn_id)
        ClinicalInfoPaths_df['phn_str'] = [phn_str] * len(ClinicalInfoPaths_df)
        # quick filtering by sentence:
        relevantSentence = 'S' + str(phn_id)[1:5] # !NOTICE: if phn_id format will change it will cause an error here!
        relevantSentence_mask = [relevantSentence == CordioFile(str(f)).sentence for f in ClinicalInfoPaths_df['file']]
        tensorWavformList = list(compress(tensorWavformList, relevantSentence_mask))
        ClinicalInfoPaths_df = ClinicalInfoPaths_df[relevantSentence_mask].reset_index(drop=True)

        # get phn_tensorWavforms_list (cut tensors):
        pb = tqdm(range(len(ClinicalInfoPaths_df)), desc='creating phoneme tensor list:', disable=True)
        phn_tensorWavforms_list = []
        validVec = []
        for i in pb:
            # getting phoneme part of wav file:
            tensorWavform = tensorWavformList[i]
            f = CordioFile(ClinicalInfoPaths_df['file'][i])
            phonemeObjs, isValidPhn = readASRJsonPhoneme(f.asrJsonPath)
            if not isValidPhn:
                validVec.append(False)
                continue
            phnObj = False
            for p in phonemeObjs:
                if p.asr_expected_id == phn_id:
                    phnObj = p
                    validVec.append(True)
                    break
            if not phnObj:
                validVec.append(False)
                continue
            phn_tensorWavform = tensorWavform[phnObj.start_sample: phnObj.end_sample]
            phn_tensorWavforms_list.append(phn_tensorWavform)
        return phn_tensorWavforms_list, ClinicalInfoPaths_df[validVec].reset_index(drop=True)

##############
# debug main #
##############
# warnings.filterwarnings(action='once')
#
# db_path = r'\\192.168.55.210\db\Acute'
# pl = ["RAM-0053", "RAM-0072", "RAM-0077",
#       "RAM-0051", "RAM-0057", "RAM-0079",
#       "RAM-0059", "RAM-0066", "RAM-0081"]
# phn_ids = ["min", "fek", "dom", "had"]
# pp = Preprocess(db_path=db_path, patient_list=pl, phonemeId_list=phn_ids, workers=-1)
# phns_tensorWavforms_and_clinicalInfoDF_dict =
# pp.getSentenceDownSampledTensorWavformList(device='cuda', downsample_rate=16000, sentence_list=None, clinicallInfo: str= 'all', get_ClinicalInfoPaths_df: bool=False, disable_pbs: bool=True):
# phns_tensorWavforms_and_clinicalInfoDF_dict = pp.getDownSampledPhnsTensorWavformList(device='cuda', downsample_rate=16000, clinicallInfo='all', disable_pbs=False)
# print("♪┏(°.°)┛┗(°.°)┓ done! ┗(°.°)┛┏(°.°)┓ ♪")
