###########
# IMPORTS #
###########
import shutil
import subprocess
import os
from pathlib import Path
import pandas as pd

from infrastructure.CordioExtractASRFeatures import readASRJsonSentence
from infrastructure.CordioFile import CordioFile
from infrastructure.patientsInformation.CordioPatientClinicalInformation import CordioClinicalInformation

sep = os.path.sep

def move_rename_audio(input_audio_url: Path, output_audio_url: Path, FLAG_move_audio_files=False):
    new_name = Path(str(output_audio_url.parent) + sep + output_audio_url.name.replace(" ", "").replace(",", ""))
    if FLAG_move_audio_files:
        command = "cp \"{audio_in}\" \"{audio_out}\"".format(audio_in=input_audio_url, audio_out=new_name)
        if not Path(output_audio_url.parent).exists():
            # If directory doesn't exist, create it
            command = "mkdir \"{parent_dir}\" && ".format(parent_dir=output_audio_url.parent) + command
        subprocess.Popen(command, shell=True)
    return Path(new_name)


def process_directory(wav_files_dir: Path, patientID: str, sentenceID: str):
    fileMask = '*.wav'
    RelevantRecordingsList = []
    for file in Path(wav_files_dir).glob(fileMask):
        if (sentenceID in file.name) and (patientID in file.name):
            RelevantRecordingsList.append(CordioFile(file))
    RelevantRecordingsList.sort(key=lambda x: x.recordingDatetime, reverse=False)
    return RelevantRecordingsList


def data_preparation_pyannote(WAV_DB_FOLDER: str, LOCAL_FOLDER: str, PATIENTS: list, setType: str = "set",
                              FLAG_move_audio_files: bool = False, flag_use_existing_files: bool = False,
                              sentenceID: str = ""):
    print("Patient embedding preprocessing")
    sep = os.path.sep

    # Make sure output folder exist
    if not os.path.isdir(LOCAL_FOLDER):
        os.mkdir(LOCAL_FOLDER)

    CSV_TRAINING_OUTPUT_PATH = LOCAL_FOLDER + sep + 'training_data_pairs'

    rttm_file_path = CSV_TRAINING_OUTPUT_PATH + "." + setType + ".rttm"
    uem_file_path = CSV_TRAINING_OUTPUT_PATH + "." + setType + ".uem"

    rttm_patient_files_list = list()
    uem_patient_files_list = list()

    # Build db from files
    for p in PATIENTS:  # Loop on patients
        rttm_file_patient_path = CSV_TRAINING_OUTPUT_PATH + "_" + p + "." + setType + ".rttm"
        uem_file_patient_path = CSV_TRAINING_OUTPUT_PATH + "_" + p + "." + setType + ".uem"

        rttm_file = pd.DataFrame(
            columns=["SPEAKER", "uri", "a1", "start", "duration", "NA1", "NA2", "identifier", "NA3", "NA4"])
        uem_file = pd.DataFrame(columns=["uri", "a1", "start", "end"])

        # Change patients string to fit Cordio database
        p_folder = p.split(sep="-")[0] + sep + p
        # Read all wav files from folders
        wav_files_list = list()
        wav_files_dir = Path(WAV_DB_FOLDER + sep + p_folder)

        # Skip patients that has the pre-processing output already
        if flag_use_existing_files and Path(rttm_file_patient_path).exists() and Path(uem_file_patient_path).exists():
            rttm_patient_files_list.append(rttm_file_patient_path)
            uem_patient_files_list.append(uem_file_patient_path)
            continue

        print(wav_files_dir)
        try:
            R = process_directory(wav_files_dir, patientID=p, sentenceID=sentenceID)
            wav_files_list.extend(R)
        except Exception as ex:
            print("An exception of type {0} occurred. Arguments:\n{1!r}".format(type(ex).__name__, ex.args))
            print("error processing folder")

        if (p_folder.find('RA') == 0):  # If Rambam patient, try to extract the data from the acute study
            # For the acute study, add the appropriate location of wav files
            p_folder_acute = "Acute" + sep + "RAM" + sep + p + sep + "cut"
            p_folder_acute = "Acute" + sep + p
            wav_files_dir_acute = Path(WAV_DB_FOLDER + sep + p_folder_acute)
            print(wav_files_dir_acute)
            try:
                R = process_directory(wav_files_dir_acute, patientID=p, sentenceID=sentenceID)
                wav_files_list.extend(R)
            except Exception as ex:
                print("An exception of type {0} occurred. Arguments:\n{1!r}".format(type(ex).__name__, ex.args))
                print("error processing folder")

        if wav_files_list.__len__() == 0:  # if file list is empty, continue
            continue

        clin1 = CordioClinicalInformation("", "")
        for r1 in wav_files_list:
            if (not r1.asrJsonPath) or (r1.wav_path.stat().st_size == 0):
                # Skip empty files
                continue
            # Update patient name and experiment name
            if "Acute" in str(r1.asrJsonPath):  # acute study
                clin1.experiment_name = "acute"
            elif "RA" in p:  # Rambam patient from the community experiment
                clin1.experiment_name = "community"
            else:  # All other patient, no need to specify experiment name, since they are only in a single experiment
                clin1.experiment_name = ""
            try:
                clin1.patient = r1.patient
            except Exception as ex:
                print("An exception of type {0} occurred. Arguments:\n{1!r}".format(type(ex).__name__, ex.args))
                print("Cannot assign clinical information to patient: " + r1.patient)
                continue

            try:
                r1_asr, r1_asr_isValid = readASRJsonSentence(r1.asrJsonPath)
            except Exception as ex:
                print("An exception of type {0} occurred. Arguments:\n{1!r}".format(type(ex).__name__, ex.args))
                print("No asr file for wav file: " + str(r1.wav_path))
                continue

            # if (not r1_asr_isValid) or ((r1_asr.duration / r1_asr.samplingRate) < 0.5):
            if (not r1_asr_isValid) or ((r1_asr.duration / r1_asr.samplingRate) < 1.5):
                # If sentence is not valid, continue
                continue
            # Trim audio file to build the db to local folder
            new_name_wav = move_rename_audio(input_audio_url=r1.wav_path,
                                             output_audio_url=Path(LOCAL_FOLDER + sep + r1.wav_path.parent.name + sep + r1.wav_path.name),
                                             FLAG_move_audio_files=FLAG_move_audio_files)

            clst1 = clin1(r1.recordingDatetime)
            rttm_file = rttm_file.append({"filePath": new_name_wav,
                                          "uri": new_name_wav.stem,
                                          "start": r1_asr.start_sample / r1_asr.samplingRate,
                                          "duration": (r1_asr.end_sample - r1_asr.start_sample) / r1_asr.samplingRate,
                                          "identifier": (p + "_cs_" + str(clst1).replace(".", "")).replace("-", "_"), "NA3": "<NA>", "NA4": "<NA>"},
                                          ignore_index=True)
            uem_file = uem_file.append({"uri": new_name_wav.stem,
                                          "a1": "1",
                                          "start": r1_asr.start_sample / r1_asr.samplingRate,
                                          "end": r1_asr.end_sample / r1_asr.samplingRate},
                                          ignore_index=True)
        try:
            rttm_file.to_csv(rttm_file_patient_path, index=False, header=False, sep=' ')
            uem_file.to_csv(uem_file_patient_path, index=False, header=False, sep=' ')
            rttm_patient_files_list.append(rttm_file_patient_path)
            uem_patient_files_list.append(uem_file_patient_path)
        except Exception as ex:
            print("While saving training data csv, an exception of type {0} occurred. Arguments:\n{1!r}".format(type(ex).__name__, ex.args))

    # Concatenate all files to single file
    with open(rttm_file_path, 'wb') as wfd:
        for f in rttm_patient_files_list:
            with open(f, 'rb') as fd:
                shutil.copyfileobj(fd, wfd)

    with open(uem_file_path, 'wb') as wfd:
        for f in uem_patient_files_list:
            with open(f, 'rb') as fd:
                shutil.copyfileobj(fd, wfd)


print("done")